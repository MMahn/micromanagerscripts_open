# README #

This repo is a collection of Micro-Manager beanshell scripts written for Micro-Manager 1.4.22 (August 14, 2015). 
Use scripts at your own risk.

### What is this repository for? ###

* Collection of Micro-Manger scripts to be be used as starting point for your own applications.
* Tested in Micro-Manager 1.4.22 (August 14, 2015).


### How do I get set up? ###

* Load a script into the Micro-Manger Script Panel.


### Contribution guidelines ###

* File names should be self-explanatory.
* Code must be well commented, to allow for easy adaptation.


### Who do I talk to? ###

* MMahn